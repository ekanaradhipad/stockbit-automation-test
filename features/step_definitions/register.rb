require 'rubygems'
require 'selenium-webdriver'

driver = Selenium::WebDriver.for :chrome

Given(/^user opens stockbit on register$/) do
  #driver = Selenium::WebDriver.for :chrome
  driver.get "https://stockbit.com"
  #sleep(1)
end

When("user clicks register") do
   driver.find_element(:xpath, "//a[@class='register-lnd']").click()
end

Then("user is redirected to register page") do
  driver.find_elements(:xpath, "//*[@class='loginlogin register-email']").size() > 0
end