require 'rubygems'
require 'selenium-webdriver'

driver = Selenium::WebDriver.for :chrome


Given("user opens stockbit homepage") do
  driver.get "https://stockbit.com"
end

When(/^user clicks "(.*)$/) do |text_name|
  case text_name
  when 'investing'
    driver.find_element(:xpath, "//a[contains(text(), 'Investing')]").click()
  when 'pro tools'
    driver.find_element(:xpath, "//a[contains(text(), 'Pro Tools')]").click()
  when 'academy'
    driver.find_element(:xpath, "//a[contains(text(), 'Academy')]").click()
  when 'about us'
    driver.find_element(:xpath, "//a[contains(text(), 'About Us')]").click()
  when 'blog'
    driver.find_element(:xpath, "//a[contains(text(), 'Blog')]").click()
  end
end

Then(/^user is redirected to "(.*) page$/) do |text_name|
  case text_name
  when 'investing'
    #
  when 'pro tools'
    driver.find_elements(:xpath, "//a[contains(text(), 'Buka Rekening Anda')]").size() > 0
  when 'academy'
    #
  when 'about us'
    driver.find_elements(:xpath, "//div[@class = 'history-info']").size() > 0
  when 'blog'
    #
  end
end