require 'rubygems'
require 'selenium-webdriver'

driver = Selenium::WebDriver.for :chrome

Given(/^user opens stockbit$/) do
  #driver = Selenium::WebDriver.for :chrome
  driver.get "https://stockbit.com"
  #sleep(1)
end


When("user clicks login") do
  driver.find_element(:xpath, "//div[contains(@class, 'button button-light')]/descendant::a[@class='login-ldn']").click()
end

Then("user is redirected to login page") do
  driver.find_elements(:xpath, "//*[@class='loginlogin']").size() > 0
end


When("user enters email") do
  driver.find_element(:xpath, "//input[contains(@id, 'username')]").send_key("mymail@gmail.com") 
end

When("user enters password") do
   driver.find_element(:xpath, "//input[contains(@id, 'username')]").send_key("password") 
end

When("User clicks Login") do
  driver.find_element(:xpath, "//input[contains(@id, 'loginbutton')]").click()
end
