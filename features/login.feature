@login
Feature: Login

Scenario: User open stockbit and goes to login page
   Given user opens stockbit
   When user clicks login
   Then user is redirected to login page

Scenario: User open stockbit and enters wrong email
   Given user opens stockbit
   When user clicks login
   Then user is redirected to login page
   When user enters email
   And user enters password
   And User clicks Login 