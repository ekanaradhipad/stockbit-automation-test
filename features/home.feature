@home
Feature: Home

Scenario: User open stockbit and goes to Investing page
   Given user opens stockbit homepage
   When user clicks "investing"
   Then user is redirected to "investing" page

Scenario: User open stockbit and goes to Investing page
   Given user opens stockbit homepage
   When user clicks "pro tools"
   Then user is redirected to "pro tools" page

Scenario: User open stockbit and goes to Investing page
   Given user opens stockbit homepage
   When user clicks "academy"
   Then user is redirected to "academy" page

Scenario: User open stockbit and goes to Investing page
   Given user opens stockbit homepage
   When user clicks "about us"
   Then user is redirected to "about us" page

Scenario: User open stockbit and goes to Investing page
   Given user opens stockbit homepage
   When user clicks "blog"
   Then user is redirected to "blog" page
